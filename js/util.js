function selectText(element) {
    var text = document.getElementById(element);
    if (document.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(text);
        selection.removeAllRanges();
        selection.addRange(range);
        /*if(selection.setBaseAndExtent){
            selection.setBaseAndExtent(text, 0, text, 1);
        }*/
    } 
}

//===============cookie操作，品种收藏===================
function setCookie(cname,cvalue,exdays){
  var d = new Date();
  d.setTime(d.getTime()+(exdays*24*60*60*1000));
  var expires = "expires="+d.toGMTString();
  var reg = new RegExp('\n', "g")
  cvalue = cvalue.replace(reg, '<br>')
  cvalue = cvalue.replace(/\t/, '')
  cvalue = cvalue.replace(/ /, '')
  document.cookie = cname + "=" + cvalue + "; " + expires  + ";path=/";
}
function getCookie(cname){
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++)   {
    var c = ca[i].trim();
	var reg = new RegExp('<br>', "g")
    if (c.indexOf(name)==0) return c.substring(name.length,c.length).replace(reg, '\n');
  }
  return "";
}
function delCookie(cname){
	document.cookie = cname+'=0;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/'
}
function favorite(){
	var isFaved = getCookie(info['name'])!=''
	if( isFaved ) {
		delCookie(info['name'])
		document.getElementById('favo').style.color=''
	} else {
		setCookie(info['name'], '1', 30)
		document.getElementById('favo').style.color='red'
	}
	document.getElementById('favo').innerHTML = isFaved?'收藏':'已收藏'
}