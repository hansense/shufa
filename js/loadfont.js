var myfonts = {}

function $(id){return document.getElementById(id);}

function setFont(family){
	if( myfonts[family] == undefined ){		
		console.log('正在装载字体：'+family)
		paint('正在装载字体：'+family)
		loadFont(family)		
	}
	paint(family)
}

function loadFont(family) {
	const font = new FontFace(
		family,
		"url(" + "./fonts/" + family + ".ttf" + ")"
	);
	font.load();
	document.fonts.add(font);	
	myfonts[family]=true
}

function paint(family, txt){	
	if(!txt){
		txt = $('area').innerText
	}
	
	var s=''
	for(var i in txt){
		if(txt[i]=='\n') s+='<br/>'
		else s += '<span onclick="anima(this)">' + txt[i] + '</span>'		
	}
	$('shu').innerHTML = s	
	
	$('shu').style.fontFamily = family
}

function anima(obj) {
	$('chazi').innerText = obj.innerText
	writer.setCharacter(obj.innerText); 
	writer.loopCharacterAnimation();
	model(true)
}