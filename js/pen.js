function pencel(obj){
	var content = obj.parentNode.parentNode
	var p_arr = new Array()
	for(var i in content.childNodes){
		if(content.childNodes[i].nodeName!='P') continue
		if(content.childNodes[i].children.length==0){
			p_arr.push(content.childNodes[i])
			continue
		}
		if(content.childNodes[i].children[0]==obj){
			p_arr.push(content.childNodes[i])
			break
		}else{
			p_arr = new Array()
		}
	}
	var s = ''
	for(var i=0; i<p_arr.length-1; i++){
		s += p_arr[i].innerText + '<br>'
	}
	s += obj.previousSibling.nodeValue
	setCookie("TEXT", s, 3)
	//var tempwindow = window.open('_blank');
	window.location = "../index.html"
}
//为所有 <h4> 动态添加返回顶部的 <a>
var h4 = document.getElementsByTagName('h4')
for(var i=1; i<h4.length; i++){
	h4[i].id = i
	var a = document.createElement("a")
	a.href = "#top"
	var img = document.createElement("img")
	img.src = '../img/top.jpg'
	img.width = 20
	img.title = '回到顶部'
	a.appendChild(img)
	h4[i].appendChild(a)
}

//为 <p> 动态添加展开字帖的 <img>
var content = document.getElementsByClassName('content')
for(var i in content){
	var nodes = content[i].childNodes
	var p = new Array()
	for(var j in nodes){
		if(nodes[j].nodeName=='P') p.push(nodes[j])
	}
	addPencel(p)
}
function addPencel(p){
	var count = 0
	for(var i=0; i<p.length; i++){	
		count += p[i].innerHTML.length	
		if(count<200 && i<p.length-1){
			var next = Math.abs(200-(count+p[i+1].innerHTML.length))
			var cur = Math.abs(200-count)
			if(next<cur) continue
		}
		let title = "展开字帖（" + count+"字）"
		var img = '<img src="../img/pen.jpg" title='+title+' onclick="pencel(this)"/>'
		p[i].innerHTML += img
		count = 0
	}
}