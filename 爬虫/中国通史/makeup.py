import json
from book import createBook

filename = 'cnhistory'
def read(index):
    f = open('cnhistory/{}{}.txt'.format(filename, index), 'r', encoding='utf-8')
    s = f.read()
    f.close()
    content = json.loads(s)
    return content
    # print(content['name'])
    # for chapter in content['chapter']:
        # print(chapter)

def makeObje():
	book = {
		'title': '中国通史',
		'alias': 'CCTV《中国通史》纪录片解说词',
		'author': '中央电视台',
		'source': 'https://www.jianshu.com/nb/43937294',
		'content':[]
	}
	for i in range(1, 94):
		content = read(i)
		# print(content['name'], len(content['chapter']))
		book['content'].append(content)
	return book
book = makeObje()
createBook(book, template='template.html', target=None)